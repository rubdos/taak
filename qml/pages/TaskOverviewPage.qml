import QtQuick 2.0
import Sailfish.Silica 1.0

import "../delegates"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaListView {
        id: taskListView
        model: tasks

        anchors.fill: parent

        ViewPlaceholder {
            //: Task overview page: empty task list label
            //% "Task list is empty"
            text: qsTrId("overview-empty list")
            enabled: tasks.count === 0
        }


        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                //: Task overview page: pull down: sync
                //% "Synchronize with server"
                text: qsTrId("overview-pull-sync")
                onClicked: tasks.sync()
            }
            MenuItem {
                //: Task overview page: pull down: create task
                //% "Create new task"
                text: qsTrId("overview-pull-create-new")
                onClicked: taakWindow.showNewTaskPage()
            }
        }

        header: PageHeader {
            //: Task overview page: header
            //% "Pending tasks"
            title: qsTrId("overview-header")
        }

        delegate: TaskOverviewItem {
            task: model
            width: ListView.view.width

            onClicked: {
                pageStack.push(Qt.resolvedUrl("CreateTaskPage.qml"), {task: model})
            }

            menu: ContextMenu {
                MenuItem {
                    text: "Done"
                    onClicked: {
                        tasks.done(id)
                    }
                }
            }
        }

        section.delegate: SilicaItem {
            width: parent.width
            height: childrenRect.height

            Label {
                x: Theme.horizontalPageMargin
                width: parent.width - 2*x
                text: section
                horizontalAlignment: Text.AlignRight
                anchors.margins: Theme.horizontalPageMargin
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.secondaryHighlightColor
                wrapMode: Text.Wrap
            }
        }
        section.property: "project"
    }
}

import QtQuick 2.0
import Sailfish.Silica 1.0

import "../components"

Dialog {
    id: createTaskPage

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All
    property var task

    property bool pending: taskStatus.currentIndex == 0

    Component.onCompleted: {
        if (task !== undefined) {
            console.log(
                        "\ntask.id: " + task.id +
                        "\ntask.description: " + task.description +
                        "\ntask.project: " + task.project +
                        "\ntask.urgency: " + task.urgency +
                        "\ntask.priority: " + task.priority +
                        "\ntask.due: " + task.due +
                        "\ntask.overdue: " + task.overdue +
                        "\ntask.color: " + task.color

                        )
            projectSelector.setProject(task.project)

            // FIXME priority, due, overdue, color are missing

        }
    }

    onAccepted: {
        var priorities = ["", "H", "M", "L"];
        if (task !== undefined) {
            tasks.edit(task.id, description.text, priorities[priority.currentIndex], dueDatePicker.dateText, scheduledDatePicker.dateText, projectSelector.project);
        } else {
            if(pending) {
                tasks.add(description.text, priorities[priority.currentIndex], dueDatePicker.dateText, scheduledDatePicker.dateText, projectSelector.project);
            } else {
                tasks.log(description.text, projectSelector.project);
            }
        }
    }

    canAccept: description.text.length > 0

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: col.height + Theme.paddingLarge

        VerticalScrollDecorator {}

        Column {
            id: col
            spacing: Theme.paddingLarge
            width: parent.width

            DialogHeader {
                title: (task !== undefined)
                       //: Edit task page header
                       //% "Edit task"
                       ? qsTrId("taak-edit-task-page-title")
                         //: Create new task page header
                         //% "Create new task"
                       : qsTrId("taak-create-task-page-title")

                acceptText:
                    (task !== undefined) ?
                    //: Create new task page: accept task changes while editing
                    //% "Edit task"
                    qsTrId("taak-create-task-page-edit") :
                    pending ?
                    //: Create new task page: accept task creation
                    //% "Create task"
                    qsTrId("taak-create-task-page-create") :
                    //: Create new task page: accept task creation: log
                    //% "Log task"
                    qsTrId("taak-create-task-page-log")
            }

            TextArea {
                id: description
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                focus: true
                //: New task page: Task description label
                //% "Description"
                label: qsTrId("taak-create-task-description")

                //: New task page: Task description placeholder
                //% "Put the trash out"
                placeholderText: qsTrId("taak-create-task-description-placeholder")
                text: (task !== undefined) ? task.description : ''
            }

            ComboBox {
                id: taskStatus

                //: New task page: Task status label
                //% "Status"
                label: qsTrId("taak-create-task-status")

                menu: ContextMenu {
                    MenuItem {
                        //: New task page: status: pending
                        //% "Pending"
                        text: qsTrId("taak-create-task-pending")
                    }

                    MenuItem {
                        //: New task page: status: completed
                        //% "Completed"
                        text: qsTrId("taak-create-task-completed")
                        color: "green"
                    }
                }
            }

            ProjectSelector {
                id: projectSelector
                visible: pending
            }

            ComboBox {
                id: priority

                //: New task page: Task priority label
                //% "Priority (optional)"
                label: qsTrId("taak-create-task-priority")

                menu: ContextMenu {
                    MenuItem {
                        //: New task page: priority: No priority
                        //% "No priority"
                        text: qsTrId("taak-create-task-priority-no-priority")
                    }
                    MenuItem {
                        //: New task page: priority: High priority
                        //% "High priority"
                        text: qsTrId("taak-create-task-priority-high")
                        color: "red"
                    }
                    MenuItem {
                        //: New task page: priority: Medium priority
                        //% "Medium priority"
                        text: qsTrId("taak-create-task-priority-medium")
                        color: "green"
                    }
                    MenuItem {
                        //: New task page: priority: Low priority
                        //% "Low priority"
                        text: qsTrId("taak-create-task-priority-low")
                        color: "blue"
                    }
                 }
            }

            // ------ BEGIN DATES ------
            SectionHeader {
                //: Create task page: dates section
                //% "Due and schedule date"
                text: qsTrId("taak-dates-section")
                visible: pending
            }

            TaskDatePicker {
                id: dueDatePicker

                //: New task page: Due date selector
                //% "Due date"
                label: qsTrId("taak-create-task-due")
                visible: pending
            }

            TaskDatePicker {
                id: scheduledDatePicker

                //: New task page: Schedule date selector
                //% "Scheduled date"
                label: qsTrId("taak-create-task-scheduled")
                visible: pending
            }
            // -------- END DATES --------
        }
    }
}

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    property int pendingTaskCount: tasks.count

    Label {
        id: pendingCount
        text: pendingTaskCount
        x: Theme.paddingLarge
        y: Theme.paddingMedium
        font.pixelSize: Theme.fontSizeHuge
    }

    Label {
        id: pendingLabel

        //: Pending tasks cover label. Code requires exact line break tag "<br/>".
        //% "Pending<br/>task(s)"
        text: qsTrId("pending-tasks", pendingTaskCount).replace("<br/>", "\n")
        font.pixelSize: Theme.fontSizeExtraSmall
        maximumLineCount: 2
        wrapMode: Text.Wrap
        fontSizeMode: Text.HorizontalFit
        lineHeight: 0.8
        height: implicitHeight/0.8
        verticalAlignment: Text.AlignVCenter
        anchors {
            right: parent.right
            left: pendingCount.right
            leftMargin: Theme.paddingMedium
            baseline: pendingCount.baseline
            baselineOffset: lineCount > 1 ? -implicitHeight/2 : -(height-implicitHeight)/2
        }
    }

    OpacityRampEffect {
        offset: 0.5
        sourceItem: pendingLabel
        enabled: pendingLabel.implicitWidth > Math.ceil(pendingLabel.width)
    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-new"
            onTriggered: {
                taakWindow.activate()
                taakWindow.showNewTaskPage(PageStackAction.Immediate)
            }
        }
    }

    Column {
        x: Theme.paddingLarge
        spacing: Theme.paddingSmall
        width: parent.width - 2*Theme.paddingLarge

        anchors {
            top: pendingLabel.bottom
            bottom: coverActionArea.top
        }

        Item {
            width: parent.width + Theme.paddingLarge
            height: parent.height - parent.spacing

            ListView {
                id: taskList

                property int taskHeight: (parent.height - Theme.paddingSmall - spacing)/2

                model: tasks

                width: parent.width
                height: 2*taskHeight + spacing
                spacing: Theme.paddingSmall
                visible: pendingTaskCount > 0

                delegate: CoverTaskItem {
                    task: model
                    height: taskList.taskHeight
                    width: taskList.width
                }
            }
            OpacityRampEffect {
                offset: 0.5
                sourceItem: taskList
            }
        }
    }

    Image {
        source: "./taskwarrior.png"
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width
        height: sourceSize.height * (parent.width / sourceSize.width)
        fillMode: Image.PreserveAspectFit
        opacity: 0.1
    }
}

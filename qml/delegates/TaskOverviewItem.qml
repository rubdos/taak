import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    id: root
    property var task

    Row {
        spacing: Theme.paddingLarge

        Rectangle {
            id: rectangle

            radius: Theme.paddingSmall/3
            width: Theme.paddingSmall
            height: Theme.itemSizeSmall

            color: task.color
        }

        Column {
            id: descriptionColumn

            Label {
                id: descriptionLabel

                height: dueLabel.visible ? (root.height * 2 / 3) : root.height
                width: root.width - rectangle.width - 3*Theme.paddingLarge
                maximumLineCount: dueLabel.visible ? 1 : 2

                text: task.description
                truncationMode: TruncationMode.Fade
            }

            Label {
                id: dueLabel

                font.pixelSize: Theme.fontSizeSmall
                verticalAlignment: Text.AlignBottom
                height: root.height / 3
                width: root.width

                color: task.overdue ? Theme.highlightColor : Theme.secondaryHighlightColor

                maximumLineCount: 1
                wrapMode: Text.Wrap

                visible: task.due !== null

                text: {
                    if (task.due !== null ) {
                        return Format.formatDate(task.due, Formatter.DurationElapsed);
                    } else {
                        return "";
                    }
                }
            }
        }
    }
}


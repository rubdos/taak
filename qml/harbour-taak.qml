import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow {
    id: taakWindow
    initialPage: Component { TaskOverviewPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    function showNewTaskPage(operationType, data) {
        if (pageStack.depth != 1) {
            pageStack.replaceAbove(null, Qt.resolvedUrl("pages/TaskOverviewPage.qml"), { }, PageStackAction.Immediate)
        }
        pageStack.push(Qt.resolvedUrl("pages/CreateTaskPage.qml"), { }, operationType)
    }

    function __translation_stub() {
        // QML-lupdate mirror for harbour-taak.profile

        //: Permission for Taskwarrior directory
        //% "Taskwarior data directory"
        var _f = qsTrId("permission-la-data");

        //: Permission description for Whisperfish data storage
        //% "Read and write Taskwarrior data"
        var _f = qsTrId("permission-la-data_description");
    }
}

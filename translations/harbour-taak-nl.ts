<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name></name>
    <message id="pending-tasks" numerus="yes">
        <source>Pending&lt;br/&gt;task(s)</source>
        <extracomment>Pending tasks cover label. Code requires exact line break tag &quot;&lt;br/&gt;&quot;.</extracomment>
        <translation>
            <numerusform>Resterende&lt;br/&gt;taak</numerusform>
            <numerusform>Resterende&lt;br/&gt;taken</numerusform>
        </translation>
    </message>
    <message id="permission-la-data">
        <source>Taskwarior data directory</source>
        <extracomment>Permission for Taskwarrior directory</extracomment>
        <translation>Taskwarrior datamap</translation>
    </message>
    <message id="permission-la-data_description">
        <source>Read and write Taskwarrior data</source>
        <extracomment>Permission description for Whisperfish data storage</extracomment>
        <translation>Lezen en schrijven van Taskwarrior gegevens</translation>
    </message>
    <message id="taak-create-task-page-title">
        <source>Create new task</source>
        <extracomment>Create new task page header</extracomment>
        <translation>Maak nieuwe taak</translation>
    </message>
    <message id="taak-create-task-description">
        <source>Description</source>
        <extracomment>New task page: Task description label</extracomment>
        <translation>Beschrijving</translation>
    </message>
    <message id="taak-create-task-description-placeholder">
        <source>Put the trash out</source>
        <oldsource>Put the trash out recur:weekly</oldsource>
        <extracomment>New task page: Task description placeholder</extracomment>
        <translation>Zet het huisvuil buiten</translation>
    </message>
    <message id="taak-create-task-priority">
        <source>Priority (optional)</source>
        <extracomment>New task page: Task priority label</extracomment>
        <translation>Prioriteit</translation>
    </message>
    <message id="taak-create-task-priority-no-priority">
        <source>No priority</source>
        <extracomment>New task page: priority: No priority</extracomment>
        <translation>Geen prioriteit</translation>
    </message>
    <message id="taak-create-task-priority-high">
        <source>High priority</source>
        <extracomment>New task page: priority: High priority</extracomment>
        <translation>Hoge prioriteit</translation>
    </message>
    <message id="taak-create-task-priority-medium">
        <source>Medium priority</source>
        <extracomment>New task page: priority: Medium priority</extracomment>
        <translation>Gemiddelde prioriteit</translation>
    </message>
    <message id="taak-create-task-priority-low">
        <source>Low priority</source>
        <extracomment>New task page: priority: Low priority</extracomment>
        <translation>Lage prioriteit</translation>
    </message>
    <message id="taak-create-task-due">
        <source>Due date</source>
        <extracomment>New task page: Due date selector</extracomment>
        <translation>Vervaldag</translation>
    </message>
    <message id="taak-create-task-scheduled">
        <source>Scheduled date</source>
        <extracomment>New task page: Schedule date selector</extracomment>
        <translation>Geplande datum</translation>
    </message>
    <message id="taak-date-selector-none">
        <source>None</source>
        <oldsource>No due date</oldsource>
        <extracomment>Date selector: No due/scheduled date</extracomment>
        <translation>Geen</translation>
    </message>
    <message id="taak-date-selector-eow">
        <source>End of week</source>
        <extracomment>Date selector: end of week</extracomment>
        <translation>Einde van de week</translation>
    </message>
    <message id="taak-date-selector-eoww">
        <source>End of work week</source>
        <extracomment>Date selector: end of work week</extracomment>
        <translation>Einde van de werkweek</translation>
    </message>
    <message id="taak-dates-section">
        <source>Due and schedule date</source>
        <extracomment>Create task page: dates section</extracomment>
        <translation>Verval- en planningsdatum</translation>
    </message>
    <message id="overview-pull-create-new">
        <source>Create new task</source>
        <extracomment>Task overview page: pull down: create task</extracomment>
        <translation>Maak nieuwe taak</translation>
    </message>
    <message id="overview-header">
        <source>Pending tasks</source>
        <extracomment>Task overview page: header</extracomment>
        <translation>Resterende taken</translation>
    </message>
    <message id="overview-pull-sync">
        <source>Synchronize with server</source>
        <extracomment>Task overview page: pull down: sync</extracomment>
        <translation>Synchroniseer met server</translation>
    </message>
    <message id="taak-date-selector-today">
        <source>Today</source>
        <extracomment>Date selector</extracomment>
        <translation>Vandaag</translation>
    </message>
    <message id="taak-date-selector-tomorrow">
        <source>Tomorrow</source>
        <extracomment>Date selector</extracomment>
        <translation>Morgen</translation>
    </message>
    <message id="taak-project-selector">
        <source>Project</source>
        <extracomment>Project selector: &quot;Project&quot;</extracomment>
        <translation>Project</translation>
    </message>
    <message id="taak-project-selector-no-project">
        <source>No project</source>
        <oldsource>None</oldsource>
        <extracomment>Project selector: No project</extracomment>
        <translation>Geen project</translation>
    </message>
    <message id="taak-project-selector-enter-project-name">
        <source>Enter project name</source>
        <extracomment>Project selector: Enter project name</extracomment>
        <translation>Projectnaam invoeren</translation>
    </message>
    <message id="taak-project-selector-project-name">
        <source>Project name</source>
        <extracomment>Project selector page: project name</extracomment>
        <translation>Projectnaam</translation>
    </message>
    <message id="taak-date-selector-select">
        <source>Select date</source>
        <extracomment>Date selector Select custom due/scheduled date</extracomment>
        <translation>Kies datum</translation>
    </message>
    <message id="taak-edit-task-page-title">
        <source>Edit task</source>
        <extracomment>Edit task page header</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-page-edit">
        <source>Edit task</source>
        <extracomment>Create new task page: accept task changes while editing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-page-create">
        <source>Create task</source>
        <extracomment>Create new task page: accept task creation</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-page-log">
        <source>Log task</source>
        <extracomment>Create new task page: accept task creation: log</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-status">
        <source>Status</source>
        <extracomment>New task page: Task status label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-pending">
        <source>Pending</source>
        <extracomment>New task page: status: pending</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-completed">
        <source>Completed</source>
        <extracomment>New task page: status: completed</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="overview-empty list">
        <source>Task list is empty</source>
        <extracomment>Task overview page: empty task list label</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name></name>
    <message id="pending-tasks" numerus="yes">
        <source>Pending&lt;br/&gt;task(s)</source>
        <extracomment>Pending tasks cover label. Code requires exact line break tag &quot;&lt;br/&gt;&quot;.</extracomment>
        <translation>
            <numerusform>Anstehende&lt;br/&gt;Aufgabe</numerusform>
            <numerusform>Anstehende&lt;br/&gt;Aufgaben</numerusform>
        </translation>
    </message>
    <message id="permission-la-data">
        <source>Taskwarior data directory</source>
        <extracomment>Permission for Taskwarrior directory</extracomment>
        <translation>Taskwarrior Datenverzeichnis</translation>
    </message>
    <message id="permission-la-data_description">
        <source>Read and write Taskwarrior data</source>
        <extracomment>Permission description for Whisperfish data storage</extracomment>
        <translation>Lesen und Schreiben von Taskwarrior-Daten</translation>
    </message>
    <message id="taak-create-task-page-title">
        <source>Create new task</source>
        <extracomment>Create new task page header</extracomment>
        <translation>Neue Aufgabe erstellen</translation>
    </message>
    <message id="taak-create-task-description">
        <source>Description</source>
        <extracomment>New task page: Task description label</extracomment>
        <translation>Beschreibung</translation>
    </message>
    <message id="taak-create-task-description-placeholder">
        <source>Put the trash out</source>
        <oldsource>Put the trash out recur:weekly</oldsource>
        <extracomment>New task page: Task description placeholder</extracomment>
        <translation>Bereinigen</translation>
    </message>
    <message id="taak-create-task-priority">
        <source>Priority (optional)</source>
        <extracomment>New task page: Task priority label</extracomment>
        <translation>Priorität (optional)</translation>
    </message>
    <message id="taak-create-task-priority-no-priority">
        <source>No priority</source>
        <extracomment>New task page: priority: No priority</extracomment>
        <translation>Keine Priorität</translation>
    </message>
    <message id="taak-create-task-priority-high">
        <source>High priority</source>
        <extracomment>New task page: priority: High priority</extracomment>
        <translation>Hohe Priorität</translation>
    </message>
    <message id="taak-create-task-priority-medium">
        <source>Medium priority</source>
        <extracomment>New task page: priority: Medium priority</extracomment>
        <translation>Mittlere Priorität</translation>
    </message>
    <message id="taak-create-task-priority-low">
        <source>Low priority</source>
        <extracomment>New task page: priority: Low priority</extracomment>
        <translation>Niedrige Priorität</translation>
    </message>
    <message id="taak-create-task-due">
        <source>Due date</source>
        <extracomment>New task page: Due date selector</extracomment>
        <translation>Fälligkeitsdatum</translation>
    </message>
    <message id="taak-create-task-scheduled">
        <source>Scheduled date</source>
        <extracomment>New task page: Schedule date selector</extracomment>
        <translation>Geplantes Datum:</translation>
    </message>
    <message id="taak-date-selector-none">
        <source>None</source>
        <oldsource>No due date</oldsource>
        <extracomment>Date selector: No due/scheduled date</extracomment>
        <translation>Kein Fälligkeitsdatum</translation>
    </message>
    <message id="taak-date-selector-eow">
        <source>End of week</source>
        <extracomment>Date selector: end of week</extracomment>
        <translation>Ende der Woche</translation>
    </message>
    <message id="taak-date-selector-eoww">
        <source>End of work week</source>
        <extracomment>Date selector: end of work week</extracomment>
        <translation>Ende der Arbeitswoche</translation>
    </message>
    <message id="taak-dates-section">
        <source>Due and schedule date</source>
        <extracomment>Create task page: dates section</extracomment>
        <translation>Fälliges und geplantes Datum</translation>
    </message>
    <message id="overview-pull-create-new">
        <source>Create new task</source>
        <extracomment>Task overview page: pull down: create task</extracomment>
        <translation>Erzeuge eine neue Aufgabe</translation>
    </message>
    <message id="overview-header">
        <source>Pending tasks</source>
        <extracomment>Task overview page: header</extracomment>
        <translation>Ausstehende Aufgaben</translation>
    </message>
    <message id="overview-pull-sync">
        <source>Synchronize with server</source>
        <extracomment>Task overview page: pull down: sync</extracomment>
        <translation>Mit Server synchronisieren</translation>
    </message>
    <message id="taak-date-selector-today">
        <source>Today</source>
        <extracomment>Date selector</extracomment>
        <translation>Heute</translation>
    </message>
    <message id="taak-date-selector-tomorrow">
        <source>Tomorrow</source>
        <extracomment>Date selector</extracomment>
        <translation>Morgen</translation>
    </message>
    <message id="taak-project-selector">
        <source>Project</source>
        <oldsource>Project (optional)</oldsource>
        <extracomment>Project selector: &quot;Project&quot;</extracomment>
        <translation>Projekt</translation>
    </message>
    <message id="taak-project-selector-no-project">
        <source>No project</source>
        <oldsource>None</oldsource>
        <extracomment>Project selector: No project</extracomment>
        <translation>Kein Projekt</translation>
    </message>
    <message id="taak-project-selector-enter-project-name">
        <source>Enter project name</source>
        <extracomment>Project selector: Enter project name</extracomment>
        <translation>Projektname eingeben</translation>
    </message>
    <message id="taak-project-selector-project-name">
        <source>Project name</source>
        <extracomment>Project selector page: project name</extracomment>
        <translation>Projektname</translation>
    </message>
    <message id="taak-date-selector-select">
        <source>Select date</source>
        <extracomment>Date selector Select custom due/scheduled date</extracomment>
        <translation>Datum auswählen</translation>
    </message>
    <message id="taak-create-task-page-create">
        <source>Create task</source>
        <extracomment>Create new task page: accept task creation</extracomment>
        <translation>Erzeuge Aufgabe</translation>
    </message>
    <message id="taak-create-task-page-log">
        <source>Log task</source>
        <extracomment>Create new task page: accept task creation: log</extracomment>
        <translation>Protokolliere Aufgabe</translation>
    </message>
    <message id="taak-create-task-status">
        <source>Status</source>
        <extracomment>New task page: Task status label</extracomment>
        <translation>Status</translation>
    </message>
    <message id="taak-create-task-pending">
        <source>Pending</source>
        <extracomment>New task page: status: pending</extracomment>
        <translation>Ausstehend</translation>
    </message>
    <message id="taak-create-task-completed">
        <source>Completed</source>
        <extracomment>New task page: status: completed</extracomment>
        <translation>Erledigt</translation>
    </message>
    <message id="taak-edit-task-page-title">
        <source>Edit task</source>
        <extracomment>Edit task page header</extracomment>
        <translation>Bearbeite Aufgabe</translation>
    </message>
    <message id="taak-create-task-page-edit">
        <source>Edit task</source>
        <extracomment>Create new task page: accept task changes while editing</extracomment>
        <translation>Bearbeite Aufgabe</translation>
    </message>
    <message id="overview-empty list">
        <source>Task list is empty</source>
        <extracomment>Task overview page: empty task list label</extracomment>
        <translation>Aufgabenliste ist leer</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name></name>
    <message id="taak-project-selector">
        <location filename="../qml/components/ProjectSelector.qml" line="29"/>
        <source>Project</source>
        <extracomment>Project selector: &quot;Project&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-project-selector-no-project">
        <location filename="../qml/components/ProjectSelector.qml" line="36"/>
        <source>No project</source>
        <extracomment>Project selector: No project</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-project-selector-enter-project-name">
        <location filename="../qml/components/ProjectSelector.qml" line="43"/>
        <source>Enter project name</source>
        <extracomment>Project selector: Enter project name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-project-selector-project-name">
        <location filename="../qml/components/ProjectSelector.qml" line="54"/>
        <source>Project name</source>
        <extracomment>Project selector page: project name</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-none">
        <location filename="../qml/components/TaskDatePicker.qml" line="23"/>
        <source>None</source>
        <extracomment>Date selector: No due/scheduled date</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-select">
        <location filename="../qml/components/TaskDatePicker.qml" line="30"/>
        <source>Select date</source>
        <extracomment>Date selector Select custom due/scheduled date</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-today">
        <location filename="../qml/components/TaskDatePicker.qml" line="36"/>
        <source>Today</source>
        <extracomment>Date selector</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-tomorrow">
        <location filename="../qml/components/TaskDatePicker.qml" line="42"/>
        <source>Tomorrow</source>
        <extracomment>Date selector</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-eow">
        <location filename="../qml/components/TaskDatePicker.qml" line="48"/>
        <source>End of week</source>
        <extracomment>Date selector: end of week</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-date-selector-eoww">
        <location filename="../qml/components/TaskDatePicker.qml" line="54"/>
        <source>End of work week</source>
        <extracomment>Date selector: end of work week</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="pending-tasks" numerus="yes">
        <location filename="../qml/cover/CoverPage.qml" line="20"/>
        <source>Pending&lt;br/&gt;task(s)</source>
        <extracomment>Pending tasks cover label. Code requires exact line break tag &quot;&lt;br/&gt;&quot;.</extracomment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message id="permission-la-data">
        <location filename="../qml/harbour-taak.qml" line="23"/>
        <source>Taskwarior data directory</source>
        <extracomment>Permission for Taskwarrior directory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="permission-la-data_description">
        <location filename="../qml/harbour-taak.qml" line="27"/>
        <source>Read and write Taskwarrior data</source>
        <extracomment>Permission description for Whisperfish data storage</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-edit-task-page-title">
        <location filename="../qml/pages/CreateTaskPage.qml" line="62"/>
        <source>Edit task</source>
        <extracomment>Edit task page header</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-page-title">
        <location filename="../qml/pages/CreateTaskPage.qml" line="65"/>
        <source>Create new task</source>
        <extracomment>Create new task page header</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-page-edit">
        <location filename="../qml/pages/CreateTaskPage.qml" line="71"/>
        <source>Edit task</source>
        <extracomment>Create new task page: accept task changes while editing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-page-create">
        <location filename="../qml/pages/CreateTaskPage.qml" line="75"/>
        <source>Create task</source>
        <extracomment>Create new task page: accept task creation</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-page-log">
        <location filename="../qml/pages/CreateTaskPage.qml" line="78"/>
        <source>Log task</source>
        <extracomment>Create new task page: accept task creation: log</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-description">
        <location filename="../qml/pages/CreateTaskPage.qml" line="88"/>
        <source>Description</source>
        <extracomment>New task page: Task description label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-description-placeholder">
        <location filename="../qml/pages/CreateTaskPage.qml" line="92"/>
        <source>Put the trash out</source>
        <extracomment>New task page: Task description placeholder</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-status">
        <location filename="../qml/pages/CreateTaskPage.qml" line="101"/>
        <source>Status</source>
        <extracomment>New task page: Task status label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-pending">
        <location filename="../qml/pages/CreateTaskPage.qml" line="107"/>
        <source>Pending</source>
        <extracomment>New task page: status: pending</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-completed">
        <location filename="../qml/pages/CreateTaskPage.qml" line="113"/>
        <source>Completed</source>
        <extracomment>New task page: status: completed</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority">
        <location filename="../qml/pages/CreateTaskPage.qml" line="129"/>
        <source>Priority (optional)</source>
        <extracomment>New task page: Task priority label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority-no-priority">
        <location filename="../qml/pages/CreateTaskPage.qml" line="135"/>
        <source>No priority</source>
        <extracomment>New task page: priority: No priority</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority-high">
        <location filename="../qml/pages/CreateTaskPage.qml" line="140"/>
        <source>High priority</source>
        <extracomment>New task page: priority: High priority</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority-medium">
        <location filename="../qml/pages/CreateTaskPage.qml" line="146"/>
        <source>Medium priority</source>
        <extracomment>New task page: priority: Medium priority</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-priority-low">
        <location filename="../qml/pages/CreateTaskPage.qml" line="152"/>
        <source>Low priority</source>
        <extracomment>New task page: priority: Low priority</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-dates-section">
        <location filename="../qml/pages/CreateTaskPage.qml" line="162"/>
        <source>Due and schedule date</source>
        <extracomment>Create task page: dates section</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-due">
        <location filename="../qml/pages/CreateTaskPage.qml" line="171"/>
        <source>Due date</source>
        <extracomment>New task page: Due date selector</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="taak-create-task-scheduled">
        <location filename="../qml/pages/CreateTaskPage.qml" line="180"/>
        <source>Scheduled date</source>
        <extracomment>New task page: Schedule date selector</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="overview-empty list">
        <location filename="../qml/pages/TaskOverviewPage.qml" line="21"/>
        <source>Task list is empty</source>
        <extracomment>Task overview page: empty task list label</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="overview-pull-sync">
        <location filename="../qml/pages/TaskOverviewPage.qml" line="31"/>
        <source>Synchronize with server</source>
        <extracomment>Task overview page: pull down: sync</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="overview-pull-create-new">
        <location filename="../qml/pages/TaskOverviewPage.qml" line="37"/>
        <source>Create new task</source>
        <extracomment>Task overview page: pull down: create task</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="overview-header">
        <location filename="../qml/pages/TaskOverviewPage.qml" line="45"/>
        <source>Pending tasks</source>
        <extracomment>Task overview page: header</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

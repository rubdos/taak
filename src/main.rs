use futures::prelude::*;
use qmetaobject::prelude::*;
use sailors::sailfishapp::QmlApp;
use taskwarrior::tokio::TokioSource;

mod tasks;

pub struct App {
    tasks: QObjectBox<tasks::TaskModel>,
}

impl Default for App {
    fn default() -> App {
        App {
            tasks: QObjectBox::new(tasks::TaskModel::default()),
        }
    }
}

impl App {
    pub async fn run(self) -> anyhow::Result<()> {
        let mut source = TokioSource::new().await;

        let tasks = self.tasks.pinned();

        while let Some(update) = source.next().await {
            log::debug!("Update triggered");
            tasks
                .borrow_mut()
                .inject_update(update, source.freeze().clone());
        }
        log::warn!("TokioSource depleted!");

        Ok(())
    }
}

fn main() {
    env_logger::init();
    let mut app = QmlApp::application("harbour-taak".into());
    let version: QString = env!("CARGO_PKG_VERSION").into();
    app.set_title("Taak".into());
    app.set_application_version(version.clone());
    // app.install_default_translator().unwrap();
    app.set_source(QmlApp::path_to("qml/harbour-taak.qml".into()));

    let state = App::default();

    app.set_object_property("tasks".into(), state.tasks.pinned());
    app.show_full_screen();

    qmeta_async::run(|| {
        qmeta_async::with_executor(|| {
            tokio::task::spawn_local(async { state.run().await.expect("application running") });
        });
        app.exec()
    })
    .unwrap()
}

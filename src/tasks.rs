use chrono::prelude::*;
use qmeta_async::*;
use qmetaobject::prelude::*;
use taskwarrior::set::{ChangeLog, TaskSet};

fn qdatetime_from_chrono<T: TimeZone>(dt: DateTime<T>) -> QDateTime {
    let dt = dt.with_timezone(&Local).naive_local();
    let date = QDate::from_y_m_d(dt.year(), dt.month() as i32, dt.day() as i32);
    let time = QTime::from_h_m_s_ms(
        dt.hour() as i32,
        dt.minute() as i32,
        Some(dt.second() as i32),
        None,
    );

    QDateTime::from_date_time_local_timezone(date, time)
}

#[derive(Default, QObject)]
pub struct TaskModel {
    base: qt_base_class!(trait QAbstractListModel),
    count: qt_property!(i32; READ row_count NOTIFY count_changed),
    count_changed: qt_signal!(),

    add: qt_method!(
        fn(
            &self,
            descr: QString,
            priority: QString,
            due: QString,
            scheduled: QString,
            project: QString,
        )
    ),
    edit: qt_method!(
        fn(
            &self,
            id: i32,
            descr: QString,
            priority: QString,
            due: QString,
            scheduled: QString,
            project: QString,
        )
    ),
    log: qt_method!(fn(&self, descr: QString, project: QString)),
    done: qt_method!(fn(&self, id: i32)),
    sync: qt_method!(fn(&self)),

    pub set: TaskSet,
}

impl TaskModel {
    pub fn inject_update(&mut self, update: ChangeLog, new_set: TaskSet) {
        log::debug!("Injected {} tasks", new_set.pending_len());
        self.begin_reset_model();
        let old_set = std::mem::replace(&mut self.set, new_set);
        self.end_reset_model();
        if self.set.pending_len() != old_set.pending_len() {
            log::debug!(
                "Previous amount was {} tasks. Triggering count_changed",
                old_set.pending_len()
            );
            self.count_changed();
        }
    }

    fn add(
        &self,
        descr: QString,
        priority: QString,
        due: QString,
        scheduled: QString,
        project: QString,
    ) {
        // TODO: make this async
        let output = std::process::Command::new("task")
            .arg("add")
            .arg(descr.to_string())
            .arg("priority:".to_string() + &priority.to_string())
            .arg("project:".to_string() + &project.to_string())
            .arg("due:".to_string() + &due.to_string())
            .arg("scheduled:".to_string() + &scheduled.to_string())
            .output()
            .expect("Execute task");
        println!("{}", String::from_utf8_lossy(&output.stdout));
        eprintln!("{}", String::from_utf8_lossy(&output.stderr));
    }

    fn edit(
        &self,
        id: i32,
        descr: QString,
        priority: QString,
        due: QString,
        scheduled: QString,
        project: QString,
    ) {
        // TODO: make this async
        let output = std::process::Command::new("task")
            .arg(id.to_string())
            .arg("modify")
            .arg(descr.to_string())
            .arg("priority:".to_string() + &priority.to_string())
            .arg("project:".to_string() + &project.to_string())
            .arg("due:".to_string() + &due.to_string())
            .arg("scheduled:".to_string() + &scheduled.to_string())
            .output()
            .expect("Execute task");
        println!("{}", String::from_utf8_lossy(&output.stdout));
        eprintln!("{}", String::from_utf8_lossy(&output.stderr));
    }

    fn log(&self, descr: QString, project: QString) {
        // TODO: make this async
        let output = std::process::Command::new("task")
            .arg("log")
            .arg(descr.to_string())
            .arg("project:".to_string() + &project.to_string())
            .output()
            .expect("Execute task");
        println!("{}", String::from_utf8_lossy(&output.stdout));
        eprintln!("{}", String::from_utf8_lossy(&output.stderr));
    }

    fn done(&self, id: i32) {
        // TODO: make this async
        let output = std::process::Command::new("task")
            .arg(id.to_string())
            .arg("done")
            .output()
            .expect("Execute task");
        println!("{}", String::from_utf8_lossy(&output.stdout));
        eprintln!("{}", String::from_utf8_lossy(&output.stderr));
    }

    #[with_executor]
    fn sync(&self) {
        tokio::spawn(async {
            let output = tokio::process::Command::new("task")
                .arg("sync")
                .output()
                .await
                .expect("Execute task");
            println!("{}", String::from_utf8_lossy(&output.stdout));
            eprintln!("{}", String::from_utf8_lossy(&output.stderr));
        });
    }
}

impl QAbstractListModel for TaskModel {
    fn row_count(&self) -> i32 {
        log::trace!("TaskModel::row_count");
        self.set.pending_len() as i32
    }

    fn data(&self, index: QModelIndex, role: i32) -> QVariant {
        log::trace!("TaskModel::data({}, {})", index.row(), role);
        let row = &self.set[index.row() as usize];
        match role {
            0 => (row.id as i32).into(),
            1 => QString::from(&row.description as &str).into(),
            2 => QString::from(row.project.as_deref().unwrap_or("")).into(),
            3 => row.urgency.into(),
            4 => match row.due {
                Some(t) => qdatetime_from_chrono(t).into(),
                None => QVariant::default(),
            },
            5 => match row.due {
                Some(t) => (t < Utc::now()).into(),
                None => QVariant::default(),
            },
            6 => {
                // Color
                let scheduled_past = match row.scheduled {
                    Some(t) => t < Utc::now(),
                    None => false,
                };
                let due_past = match row.due {
                    Some(t) => t < Utc::now(),
                    None => false,
                };
                if due_past {
                    QString::from("red").into()
                } else if scheduled_past {
                    QString::from("blue").into()
                } else {
                    QString::from("white").into()
                }
            }
            _ => unreachable!(),
        }
    }

    fn role_names(&self) -> std::collections::HashMap<i32, QByteArray> {
        [
            "id",
            "description",
            "project",
            "urgency",
            "due",
            "overdue",
            "color",
        ]
        .iter()
        .enumerate()
        .map(|(i, x)| (i as i32, QByteArray::from(*x)))
        .collect()
    }
}
